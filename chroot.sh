#!/bin/bash
# $1 = hardware (vbox or vmware)
# $2 = desktop (kde, gnome, xfce)
# $3 = password

echo '' && echo ''
echo '************************************************'
echo '**************** Configure GRUB'
echo '************************************************'
echo '' && echo ''
#si pas LTS:
mkinitcpio -p linux
#si LTS:
#mkinitcpio -p linux-lts
grub-mkconfig -o /boot/grub/grub.cfg

if [ "$1" == "vbox" ]
then
    grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=arch_grub --recheck
    mkdir /boot/EFI/boot
    cp /boot/EFI/arch_grub/grubx64.efi /boot/EFI/boot/bootx64.efi
fi

if [ "$1" == "vmware" ]
then 
#    grub-install /dev/sda
    grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=arch_grub --recheck
    mkdir /boot/EFI/boot
    cp /boot/EFI/arch_grub/grubx64.efi /boot/EFI/boot/bootx64.efi
fi

echo '' && echo ''
echo '************************************************'
echo '**************** Machine Settings'
echo '************************************************'
echo '' && echo ''
echo "archlinux" > /etc/hostname
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "en_DK.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' >> /etc/locale.conf
echo 'LC_TIME=en_DK.UTF-8' >> /etc/locale.conf
echo 'KEYMAP="fr-pc"' >> /etc/vconsole.conf
echo 'CONSOLEFONT="lat9w-16"' >> /etc/vconsole.conf
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
echo -en "$3\n$3" | passwd
sed -i 's!# %wheel ALL=(ALL) NOPASSWD: ALL!%wheel ALL=(ALL) NOPASSWD: ALL!g' /etc/sudoers


echo '' && echo ''
echo '************************************************'
echo '**************** Configure Pacman'
echo '************************************************'
echo '' && echo ''
sed -i 's%#TotalDownload%TotalDownload\nILoveCandy%g' /etc/pacman.conf
sed -i 's%#Color%Color%g' /etc/pacman.conf
echo '[multilib]' >> /etc/pacman.conf
echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf
echo '' >> /etc/pacman.conf
#echo '[archlinuxfr]' >> /etc/pacman.conf
#echo 'SigLevel = Never ' >> /etc/pacman.conf
#echo 'Server = http://repo.archlinux.fr/$arch ' >> /etc/pacman.conf
sed -i 's%#IgnorePkg   =%IgnorePkg = postgresql postgresql-libs %g' /etc/pacman.conf

echo '' && echo ''
echo '************************************************'
echo '**************** Install Packages'
echo '************************************************'
echo '' && echo ''
local packages=''
packages+=' xorg-server xorg-xinit xorg-apps xf86-input-mouse xf86-input-keyboard xf86-input-libinput xdg-user-dirs mesa'

if [ "$1" == "vbox" ]
then
    #si pas LTS:
    packages+=' virtualbox-guest-utils virtualbox-guest-modules-arch xf86-video-vesa'
    #si LTS:
    #packages+=' virtualbox-guest-utils linux-lts-headers virtualbox-guest-dkms xf86-video-vesa'
fi

if [ "$1" == "vmware" ]
then 
    packages+=' open-vm-tools xf86-video-vmware'
fi

if [ "$2" == "kde" ]
then 
    packages+=' plasma kde-applications'
    packages+=' kdebase-runtime' #tools to confiure KDE like kpackagetool5 (plasmapkg2)
    packages+=' latte-dock'
    packages+=' variety'
fi

if [ "$2" == "gnome" ]
then
    packages+=' gnome'
    #packages+=' lxdm-gtk3' # I use lxdm instead of GDM to get animation: https://www.reddit.com/r/linux/comments/97ph3v/gnome_on_xorg_animations_missing_with_gdm_working/ https://bugzilla.gnome.org/show_bug.cgi?id=749390
    packages+=' dconf-editor gnome-weather gnome-tweaks' # packages in gnome-extra and not in gnome group
fi

if [ "$2" == "xfce" ]
then 
    packages+=' lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings'
    packages+=' openbox obconf'
    packages+=' gvfs' #to have a Trash
    packages+=' xfce4 xfce4-goodies'
    packages+=' docky'
    packages+=' variety'
    packages+=' volumeicon'
    packages+=' network-manager-applet'
    packages+=' xcape'
    packages+=' catfish'
    packages+=' baobab' # analyze disk space
    packages+=' gnome-calculator'
    packages+=' gconf' # to configure docky
fi

packages+=' ntp wget git bash-completion'
packages+=' arc-gtk-theme materia-gtk-theme papirus-icon-theme'
packages+=' ttf-font-awesome ttf-dejavu ttf-roboto'
packages+=' screenfetch'
#packages+=' vlc'
packages+=' simplescreenrecorder'
packages+=' icedtea-web'
#packages+=' jre8-openjdk'
#packages+=' libreoffice-fresh'
#packages+=' chromium'
packages+=' bleachbit'
#packages+=' python-pyqtgraph python-pyqt5'
#packages+=' appmenu-gtk-module appmenu-qt4 libdbusmenu-glib libdbusmenu-gtk3 libdbusmenu-gtk2' # used for AppMenu
pacman -Syy --noconfirm $packages


echo '' && echo ''
echo '************************************************'
echo '**************** Create User (needs to be after vboxsf creation)'
echo '************************************************'
echo '' && echo ''
if [ "$1" == "vbox" ]; then vboxsf=",vboxsf"; fi
if [ "$1" == "vmware" ]; then vboxsf=""; fi
useradd -m -s /bin/bash -G adm,systemd-journal,wheel,games,network,video,audio,optical,floppy,storage,scanner,power,input$vboxsf -c "Eric" eric
echo -en "$3\n$3" | passwd "eric"
chmod -R 777 /scripts/

echo '' && echo ''
echo '************************************************'
echo '**************** Install AUR'
echo '************************************************'
echo '' && echo ''
su - eric -c "cd /home/eric && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -csi --noconfirm --skippgpcheck && cd .."
rm -rf /home/eric/yay

if [ "$2" == "kde" ]
then 
    su eric -c "yay -Syu --noconfirm plasma5-applets-active-window-control"
    su eric -c "yay -Syu --noconfirm numix-circle-icon-theme-git" # numix-icon-theme-git"
    su eric -c "yay -Syu --noconfirm paper-icon-theme-git"
fi

if [ "$2" == "gnome" ]
then
    su eric -c "yay -Syu --noconfirm gnome-shell-extension-dash-to-dock"
#    su eric -c "yay -Syu --noconfirm gnome-terminal-transparency"
#    su eric -c "yay -Syu --noconfirm gnome-shell-extension-topicons-plus-git"
#    su eric -c "yay -Syu --noconfirm gnome-shell-extension-no-title-bar"
#    su eric -c "yay -Syu --noconfirm capitaine-cursors"
fi

if [ "$2" == "xfce" ]
then 
    su eric -c "yay -Syu --noconfirm compton-tryone-git"
    su eric -c "yay -Syu --noconfirm skippy-xd"
    su eric -c "yay -Syu --noconfirm mugshot"
    su eric -c "yay -Syu --noconfirm gksu" # to open gtk app (thunar) as root
    #su eric -c "yay -Syu --noconfirm vala-panel-appmenu-translations-git"
    #rm /usr/share/vala-panel/applets/appmenu.plugin
    #su eric -c "yay -Syu --noconfirm vala-panel-appmenu-xfce-git"
    #su eric -c "yay -Syu --noconfirm vala-panel-appmenu-registrar-git"
    pacman -Rs --noconfirm budgie-desktop  # this has been installed by vala-panel. I remove it.
fi

#su eric -c "yay -Syu --noconfirm pamac-aur"
#su eric -c "yay -Syu --noconfirm vscodium-bin"

echo '' && echo ''
echo '************************************************'
echo '**************** Systemctl'
echo '************************************************'
echo '' && echo ''
systemctl enable NetworkManager
systemctl enable ntpd
if [ "$1" == "vbox" ]; then systemctl enable vboxservice; fi
if [ "$1" == "vmware" ]
then 
    systemctl enable vmtoolsd.service
    systemctl enable vmware-vmblock-fuse.service
fi

if [ "$2" == "kde" ]
then 
    systemctl enable sddm.service
fi

if [ "$2" == "gnome" ]
then
    systemctl enable gdm.service
    #systemctl enable lxdm.service # I use lxdm instead of GDM on vbox to get animation: https://www.reddit.com/r/linux/comments/97ph3v/gnome_on_xorg_animations_missing_with_gdm_working/
fi

if [ "$2" == "xfce" ]
then 
    systemctl enable lightdm.service
fi

systemctl enable sshd

echo '------------------------------------------------'
echo '------------------------------------------------'
echo '---------------- Global Settings'
echo '------------------------------------------------'
echo '------------------------------------------------'

cp -R /scripts/common/root/. /
cp -R /scripts/$2/root/. /

if [ "$2" == "kde" ]
then 
    mkdir /etc/sddm.conf.d/
    sddm --example-config > /etc/sddm.conf.d/sddm.conf
    sed -i 's%Current=%Current=breeze%g' /etc/sddm.conf.d/sddm.conf
    sed -i 's%CursorTheme=%CursorTheme=breeze_cursors%g' /etc/sddm.conf.d/sddm.conf
fi

if [ "$2" == "gnome" ]
then
    echo ""
    # for now I remove Wayland on Vbox as mouse is laggy (uses same thread process as Shell animation: https://bugzilla.gnome.org/show_bug.cgi?id=745032 and https://wiki.archlinux.org/index.php/GDM#Use_Xorg_backend)
    #sed -i 's%#WaylandEnable=false%WaylandEnable=false%g' /etc/gdm/custom.conf
    #sed -i 's%gtk_theme=Adwaita%gtk_theme=Adapta-Eta%g' /etc/lxdm/lxdm.conf
    #sed -i 's%# session=/usr/bin/startlxde%session=/usr/bin/gnome-session%g' /etc/lxdm/lxdm.conf
fi

if [ "$2" == "xfce" ]
then 
    rm /usr/share/xsessions/openbox*
fi

echo '------------------------------------------------'
echo '------------------------------------------------'
echo '---------------- User Settings'
echo '------------------------------------------------'
echo '------------------------------------------------'

su eric -c "cp -R /scripts/common/home/. /home/eric"
su eric -c "cp -R /scripts/$2/home/. /home/eric"

if [ "$2" == "kde" ]
then 
    su eric -c "kpackagetool5 -i /scripts/kde/mymacsimize/"
    su eric -c "kpackagetool5 -i /scripts/kde/org.communia.apptitle--v1.1.org.communia.apptitle.plasmoid"
fi

if [ "$2" == "gnome" ]
then
    echo ""
fi

if [ "$2" == "xfce" ]
then 
    echo ""
fi

if [ "$1" == "vmware" ]
then 
    cp -R /scripts/vmware/root/. /
    su eric -c "mkdir /home/eric/LEMANE-7W"
    systemctl enable share-vmware-folder.service 
fi

echo '************************************************'
echo '**************** To DO: run ~/settings.sh'
echo '************************************************'
