# Start a VM with latest archlinux ISO from https://www.archlinux.org/download/

# Might need to make the gitlab Public (in Settings)

`loadkeys fr-pc` # i.e.: loqdkeys fr)pc

`wget https://gitlab.com/eric.leman/installarch/-/archive/master/installarch-master.tar.gz`

`tar -xf installarch-master.tar.gz`

`cd installarch-master`

`chmod 777 install.sh`

`./install.sh [hardware] [desktop environment] [password]`

hardware: vbox, vmware

desktop environment: kde, gnome, xfce

password: password for root and user



For Internet at work:

`gsettings set org.gnome.system.proxy mode 'manual'`

`gsettings set org.gnome.system.proxy.http host 'proxy-emea.nslb.ad.XXXX.net'`

`gsettings set org.gnome.system.proxy.http port 80`

`gsettings set org.gnome.system.proxy.ftp host 'proxy-emea.nslb.ad.XXX.net'`

`gsettings set org.gnome.system.proxy.ftp port 80`

`gsettings set org.gnome.system.proxy.https host 'proxy-emea.nslb.ad.XXX.net'`

`gsettings set org.gnome.system.proxy.https port 80`

`gsettings set org.gnome.system.proxy ignore-hosts "['localhost', '127.0.0.0/8', '10.0.0.0/8', '192.168.0.0/16', '172.16.0.0/12' , '*.localdomain.com' ]"`

and then chome needs to get MA proxy:
https://superuser.com/questions/1083766/how-do-i-deal-with-neterr-cert-authority-invalid-in-chrome


For Wifi on USB Dongle:

`yay -S rtl8821au-dkms-git`

`sudo pacman -S linux-headers`

Parfois, le dongle Wifi ne fonctionne plus (souvent après une màj du kernel linux). Donc parfois je passe sur linux-lts.

https://unix.stackexchange.com/questions/284617/how-to-switch-arch-linux-to-lts-kernel

https://wiki.archlinux.org/index.php/Wireless_network_configuration




