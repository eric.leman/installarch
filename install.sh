#!/bin/bash

# $1 = hardware (vbox or vmware)
# $2 = desktop (kde, gnome, xfce)
# $3 = password
echo '************************************************'
echo '************************************************'
echo '**************** Start Installation'
echo '************************************************'
echo '************************************************'
if [ "$1" == "vbox" ]
then
    parted -s /dev/sda mklabel gpt \
      mkpart ESP fat32 1 512M \
      mkpart primary linux-swap 512M 4G \
      mkpart primary ext3 4G 100% \
      set 1 boot on
    mkfs.fat -F32  /dev/sda1
fi

if [ "$1" == "vmware" ]
then
    parted -s /dev/sda mklabel msdos \
      mkpart primary ext3 1 512M \
      mkpart primary linux-swap 512M 4G \
      mkpart primary ext3 4G 100% \
      set 1 boot on
    mkfs.ext4 /dev/sda1
fi

mkfs.ext4 -F /dev/sda3

mkswap /dev/sda2
swapon /dev/sda2

mount /dev/sda3 /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot

cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
sed -i 's%Server%#Server%g' /etc/pacman.d/mirrorlist
sed -i 's%#Server = http://mir.archlinux.fr/$repo/os/$arch%Server = http://mir.archlinux.fr/$repo/os/$arch%g' /etc/pacman.d/mirrorlist

echo '' && echo ''
echo '************************************************'
echo '**************** PacStrapping Package'
echo '************************************************'
echo '' && echo ''
#si pas LTS:
pacstrap /mnt base base-devel mc mtools dosfstools lsb-release ntfs-3g exfat-utils syslog-ng grub os-prober sudo wget openssh
#si LTS:
#pacstrap /mnt base base-devel mc mtools dosfstools lsb-release ntfs-3g exfat-utils syslog-ng  grub os-prober sudo wget openssh linux-lts
if [ "$1" == "vbox" ]; then pacstrap /mnt efibootmgr; fi

genfstab -U -p /mnt >> /mnt/etc/fstab


echo '' && echo ''
echo '************************************************'
echo '**************** CHROOTing'
echo '************************************************'
echo '' && echo ''
cp ./chroot.sh /mnt
chmod 777 /mnt/chroot.sh
cp -R ./scripts /mnt
arch-chroot /mnt ./chroot.sh "$1" "$2" "$3" |& tee /mnt/install.log
rm /mnt/chroot.sh
rm -rf /mnt/scripts

echo '' && echo ''
echo '************************************************'
echo '**************** end of CHROOT, rebooting'
echo '************************************************'
echo '' && echo ''
umount -R /mnt
systemctl reboot
