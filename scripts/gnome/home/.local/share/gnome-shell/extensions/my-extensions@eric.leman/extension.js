const Meta = imports.gi.Meta;
const St = imports.gi.St;
const Main = imports.ui.main;
const Panel = imports.ui.panel;


// functions used for Maximize in New Workspace:
function getWorkspaceIndex(w) {return w.get_meta_window().get_workspace().index()}

function check(act) {
  const win = act.meta_window;
  if (win.window_type !== Meta.WindowType.NORMAL)
    return;
  if (win.get_maximized() !== Meta.MaximizeFlags.BOTH)
    return;
  if (win.get_workspace().list_windows().length == 1)
    return;
  let newWorkspace = Math.max.apply(null,global.get_window_actors().map(getWorkspaceIndex)) + 1;
  win.change_workspace_by_index(newWorkspace, false)
  //destination workspace:
  global.workspace_manager.get_workspace_by_index(newWorkspace).activate(global.get_current_time());

}

const _handles = [];

// functions used for Change "Activities" to Arch logo:
function showOW(){Main.overview.toggle()}


function enable() {
  // Maximize in New Workspace
  global.get_window_actors().forEach(check);
  _handles.push(global.window_manager.connect('map', (_, act) => check(act)));
  _handles.push(global.window_manager.connect('size-change', (_, act, change) => {
    if (change === Meta.SizeChange.MAXIMIZE)
      check(act);
  }));
  _handles.push(global.window_manager.connect('switch-workspace', () => {
    const acts = global.get_window_actors()
      .filter(a => a.meta_window.has_focus());
    if (acts.length)
      check(acts[0]);
  }));

  // Change "Activities" to Arch logo
  archIcon = new St.Icon({ icon_name: 'distributor-logo-archlinux', style_class: 'system-status-icon' })
  archButton = new St.Bin({ style_class: 'panel-button',reactive: true,can_focus: true, x_fill: true,y_fill: false,track_hover: true });
  archButton.set_child(archIcon);
  archButton.connect('button-press-event', showOW);
  Main.panel._leftBox.get_first_child().add_actor(archButton);
}

function disable() {
  // Maximize in New Workspace
  _handles.splice(0).forEach(h => global.window_manager.disconnect(h));
}
