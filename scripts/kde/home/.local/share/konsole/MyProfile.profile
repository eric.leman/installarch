[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=Breeze
Font=DejaVu Sans Mono,9,-1,0,50,0,0,0,0,0,Book
UseFontLineChararacters=false

[General]
Name=MyProfile
Parent=FALLBACK/

[Interaction Options]
AutoCopySelectedText=true

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=2
ScrollBarPosition=2
